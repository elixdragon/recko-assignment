package com.recko.assignment.service.impl;

import com.recko.assignment.dao.PersonRepository;
import com.recko.assignment.entity.Person;
import com.recko.assignment.entity.model.FamilyGrouped;
import com.recko.assignment.entity.model.FamilyPowerUniverse;
import com.recko.assignment.service.api.PersonService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Harman Singh on 31 May, 2018; 14:45 IST
 */
@Service
@Slf4j
public class PersonServiceImpl implements PersonService {

  private final PersonRepository personRepository;

  @Autowired
  public PersonServiceImpl(PersonRepository personRepository) {
    this.personRepository = personRepository;
  }

  @Override
  public List<String> findFamiliesInUnverse(String universeId) {
    log.info("Finding all families in unvierse: {}", universeId);
    return personRepository.findAllFamilies(universeId);
  }

  @Override
  public List<FamilyPowerUniverse> findUnbalancedFamilies() {
    log.info("Finding unbalanced families");
    List<FamilyGrouped> familyPowers = personRepository.findFamilyPowers();
    return familyPowers.stream().collect(Collector.of(
        () -> new HashMap<String, Map<String, Long>>(),
        (scoreMap, item) -> {
          Map<String, Long> universeMap = scoreMap
              .getOrDefault(item.getFamilyId(), new HashMap<>());
          universeMap.put(item.getUniverseId(), item.getPower());
          scoreMap.put(item.getFamilyId(), universeMap);
        },
        (map1, map2) -> {
          map1.putAll(map2);
          return map1;
        },
        result -> {
          List<FamilyPowerUniverse> familyPowerUniverseList = new ArrayList<>();
          result.forEach((familyIdKey, universePowerMap) -> {
            AtomicBoolean balanced = new AtomicBoolean(true);
            Long powerShouldBe = universePowerMap.get(universePowerMap.keySet().toArray()[0]);
            universePowerMap.forEach((universe, totalPower) -> {
              if (!totalPower.equals(powerShouldBe)) {
                balanced.set(false);
              }
            });
            if (!balanced.get()) {
              familyPowerUniverseList
                  .add(FamilyPowerUniverse.builder().familyId(familyIdKey).powerInUnverse(universePowerMap)
                      .build());
            }
          });
          return familyPowerUniverseList;
        }
    ));
  }

  @Override
  public List<Person> balanceFamilies() {

    List<FamilyPowerUniverse> unbalancedFamilies = findUnbalancedFamilies();
    List<Person> peopleToAdd = new ArrayList<>();
    log.info("Balancing families");
    unbalancedFamilies.forEach(family -> {
      // make all families equal to the power of any family
      Map<String, Long> powerInUnverse = family.getPowerInUnverse();
      Long powerShouldBe = powerInUnverse.get(powerInUnverse.keySet().toArray()[0]);
      powerInUnverse.forEach(
          (universeId, currentPower) -> {
            if (!currentPower.equals(powerShouldBe)) {
              log.debug("Adding power: {}, to universe: {}, family id: {}", powerShouldBe - currentPower, universeId, family.getFamilyId());
              peopleToAdd.add(Person.builder()
                  .familyId(family.getFamilyId())
                  .power(powerShouldBe - currentPower)
                  .universeId(universeId)
                  .build()
              );
            }
          }
      );
    });

    return personRepository.saveAll(peopleToAdd);
  }

  @Override
  public Person save(Person person) {
    return personRepository.save(person);
  }


}
