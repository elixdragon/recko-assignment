package com.recko.assignment.rest.web.model.response;

import static lombok.AccessLevel.PRIVATE;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Harman Singh on 31 May, 2018; 14:36 IST
 */
@Data
@AllArgsConstructor(access = PRIVATE)
public class BaseResponse<T> {

  private String message;

  private T data;

  private Date serverTime;

  public static <K> BaseResponse<K> construct(String message, K data) {
    return new BaseResponse<>(message, data, new Date());
  }

}
