package com.recko.assignment.rest.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import com.recko.assignment.entity.Person;
import com.recko.assignment.entity.model.FamilyPowerUniverse;
import com.recko.assignment.rest.web.model.response.BaseResponse;
import com.recko.assignment.service.api.PersonService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Harman Singh on 31 May, 2018; 14:24 IST
 */
@RestController
@RequestMapping(value = "/multiverse")
@Slf4j
public class PersonController {

  final
  PersonService personService;

  @Autowired
  public PersonController(PersonService personService) {
    this.personService = personService;
  }

  ///////////////////////////////GET ALL FAMILIES////////////////////////////////////
  @ApiOperation(value = "Get all families in a universe", notes = "")
  @RequestMapping(method = GET, value = "/families")
  public BaseResponse<?> getAllFamilies(
      @RequestParam(value = "universeId") String universeId
  ) {
    List<String> familiesInUnverse = personService.findFamiliesInUnverse(universeId);
    log.info("Returning with status code: {}", 200);
    return BaseResponse.construct("SUCCESS", familiesInUnverse);
  }

  ///////////////////////////////CHECK BALANCE////////////////////////////////////

  @ApiOperation(value = "Check if families with same identifiers have same power in all universes", notes = "")
  @RequestMapping(method = GET, value = "/families/unbalanced")
  public BaseResponse<?> checkFamilyBalance() {
    List<FamilyPowerUniverse> unbalancedFamilies = personService.findUnbalancedFamilies();
    String message = "BALANCED";
    if (unbalancedFamilies.size() > 0) {
      message = "UNBALANCED";
    }
    log.info("Returning with status code: {}, message: {}", 200, message);
    return BaseResponse.construct(message, unbalancedFamilies);
  }

  ///////////////////////////////BALANCE FAMILIES////////////////////////////////////
  @ApiOperation(value = "Find unbalanced families and balance them", notes = "Returns the people that were added")
  @RequestMapping(method = GET, value = "/families/balance")
  public BaseResponse<?> balanceFamilies() {
    List<Person> peopleAdded = personService.balanceFamilies();
    log.info("Returning with status code: {}, message: {}", 200, "SUCCESS");
    return BaseResponse.construct("SUCCESS", peopleAdded);
  }

  ///////////////////////////////ADD A PERSON////////////////////////////////////

  @ApiOperation(value = "Add a person")
  @RequestMapping(method = POST, value = "/families")
  public BaseResponse<?> addPerson(
      @RequestBody Person person
  ) {
    Person savedPerson = personService.save(person);
    log.info("Returning with status code: {}, message: {}", 200, "SUCCESS");
    return BaseResponse.construct("SUCCESS", savedPerson);
  }

}
