package com.recko.assignment.entity.model;

import com.recko.assignment.entity.Person;
import java.util.List;
import lombok.Data;

/**
 * To be used in a later version, maybe
 */
@Data
public class Family {

  private String familyId;

  private List<Person> memberList;

}
