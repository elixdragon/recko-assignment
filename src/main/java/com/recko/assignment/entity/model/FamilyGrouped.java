package com.recko.assignment.entity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Harman Singh on 31 May, 2018; 14:58 IST
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FamilyGrouped {

  private String familyId;
  private String universeId;
  private Long power;

}
